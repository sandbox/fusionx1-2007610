<?php
class TableStorageHelper
	{
		const uriTemplate = "http://%s.table.core.windows.net/%s";
		
		public static function GetResponse($account_name, 
				$shared_key, 
				$table_name, 
				$page_size, 
				&$next_partition, 
				&$next_row){
 
			//Get the base uri
			$service_uri = self::getUri($account_name, $table_name);
			
					
			//Limit the results by the pageSize
			$query = '';
			if ($page_size > 0)
				$query = '?$top='.$page_size;

			//Add a filter for paging (if the partition and row were passed
			if (strlen($next_partition) > 0 && strlen($next_row) > 0)
			{
				//See if we have already created a query
				if (strlen($query) > 0)
					$query .= '&';
				else
					$query .= '?';
				
				//Add the NextPartitionKey and NextRowKey QueryString parameters
				$query .= 'NextPartitionKey'.'='.rawurlencode($next_partition);
				$query .= '&NextRowKey'.'='.rawurlencode($next_row);
			}

			$service_uri .= $query;

			//Get the date (GMT)
			$date = self::GetGMTDate();
			
			//Get the signature
			$signed_message = self::GetSignature($date, $account_name, $table_name, $shared_key);
			
			//Get the http headers array
			$headers = self::GetHttpHeaders($date, $signed_message);
						
			$session = curl_init($service_uri);
			curl_setopt($session, CURLOPT_HEADER, false);

			curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
			
			$response = curl_exec($session);
			//file_put_contents("pop.xml", $response);
			//$xml = simplexml_load_string($response);
            echo $response;
            curl_close($session);
		}
		

		static function http_parse_headers($headers=false){
			if($headers === false){
				return false;
			}
			
			$headers = str_replace("\r", "", $headers);
			$headers = explode("\n",$headers);
			
			foreach($headers as $value){
				$header = explode(": ", $value);

				if(count($header) == 1){
					$headerdata['status'] = $header[0]; 
				}
				elseif($header[0] && $header[1]){
					$headerdata[$header[0]] = $header[1];
				}
			}
			
			return $headerdata;
		}


		public static function GetGMTDate(){
			return gmdate('D, d M Y H:i:s \G\M\T');
		}
		
		public static function GetHttpHeaders($date, $signed_message){
			return array("x-ms-date:$date", 
						 "Authorization:$signed_message", 
						 "Accept:application/atom+xml, 
						 application/xml");
		}
		
		public static function GetSignature($date, $account_name, $table_name, $key){
					
			$message_to_sign = "$date\n/$account_name/$table_name";
			$signed_message = base64_encode(hash_hmac('sha256', 
													  $message_to_sign, 
													  base64_decode($key),true));
			
			return "SharedKeyLite $account_name:".$signed_message;
		}
		
		public static function getUri($account_name, $table_name){
			return sprintf(self::uriTemplate, $account_name, $table_name);
		}		
	}
	
	

?>