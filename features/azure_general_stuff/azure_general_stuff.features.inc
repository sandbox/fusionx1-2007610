<?php
/**
 * @file
 * azure_general_stuff.features.inc
 */

/**
 * Implements hook_node_info().
 */
function azure_general_stuff_node_info() {
  $items = array(
    'dataset' => array(
      'name' => t('Dataset'),
      'base' => 'node_content',
      'description' => t('dataset'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
