<?php

/**
 * @file
 * Class definition of AzureTableProcessor.
 */

/**
 * Creates simple table records from feed items. Uses Data module.
 */
 
class AzureTableProcessor extends FeedsProcessor {

  // We aren't creating or updating entities, just table records.
  // So these are empty.
  public function entityType() {}
  protected function newEntity(FeedsSource $source) {}
  protected function entitySave($entity) {}
  protected function entityDeleteMultiple($entity_ids) {}

  /**
   * Implementation of FeedsProcessor::process().
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    // Count number of created and updated nodes.
    $inserted  = $updated = 0;
    $expiry_time = $this->expiryTime();
    $this->feed_nid = $source->feed_nid;

    // TODO: NEED to get Azure Table Name based off feeds_nid
    $table = $this->table($source);

    while ($data = $parser_result->shiftItem()) {
        AzureTableProcessor::dkan_entity_save($data, $this->tableName($source));
    }
  }
  /**
   * Count items imported by this source.
   */
  public function itemCount(FeedsSource $source) {
    // TODO: Determine the count in the Azure Table.
  }

  /**
   * Implementation of FeedsProcessor::clear().
   *
   * Delete all data records for feed_entity_id in this table.
   */
  public function clear(FeedsSource $source) {
    // TODO: Determine the count in the Azure Table.
  }


  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'mappings' => array(),
      'delete_with_source' => FALSE,
      'azure_secret_key' => "Fill in your secret key here.",
      'azure_account_name' => "Fill in your account name here.",
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {

    $form['azure_account_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Azure Account Name'),
      '#description' => t('Azure Account Name can be obtained by subscribing to windows azure by creating a storage account.'),
      '#default_value' => $this->config['azure_account_name'],
    );
    
    $form['azure_secret_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Azure Secret Key'),
      '#description' => t('Azure Secret Key is generated automatically upon creation of a storage account, this consist of 64 alphanumeric.'),
      '#default_value' => $this->config['azure_secret_key'],
    );

   return $form;
  }

  /**
   * Reschedule if expiry time changes.
   */
  public function configFormSubmit(&$values) {
    parent::configFormSubmit($values);
  }

  /**
   * Return the data table name for this feed.
   */
  protected function tableName(FeedsSource $source) {
    // TODO: Determine the Azure Table name.
    return 'feedsdatastore' . $source->feed_nid;
  }

  /**
   * Return the data table for this feed.
   *
   * @throws Exception $e
   *   Throws this exception if a table cannot be found and cannot be created.
   *
   * @todo Make *Data module* throw exception when table can't be found or
   *   can't be created.
   */
  protected function table(FeedsSource $source) {
  $tableRestProxy = AzureTableProcessor::dkan_table_store_connection();
    // TODO: Create and / or retrieve info from Azure Table.
    try{  
       $tableRestProxy->getTable($this->tableName($source));
    }catch(WindowsAzure\Common\ServiceException $e) {
       $code = $e->getCode();
       $error_message = $e->getMessage();
       if ($code == 404) { 
         //table doestn exist
         $table = AzureTableProcessor::dkan_create_table($this->tableName($source));
         return $this->tableName($source);
       }
       elseif ($code == 409) { 
         //table exist
         return $this->tableName($source);
       }
    }  
  }
  

function dkan_entity_save($data, $feed_nid) {
  $tableRestProxy = AzureTableProcessor::dkan_table_store_connection();

  $nowDT = new DateTime('now', new DateTimeZone('UTC'));
  $partitionKey = $nowDT->format("mdY");
  $rowKey = uniqid();

  $entity = new WindowsAzure\Table\Models\Entity();
  $entity->setPartitionKey($feed_nid);
  $entity->setRowKey($rowKey);

  foreach ($data as $key => $val) {
    $entity->addProperty($key, WindowsAzure\Table\Models\EdmType::STRING, $data[$key]);
  }
  try {
    $tableRestProxy->insertOrReplaceEntity($feed_nid, $entity);
  } catch (WindowsAzure\Common\ServiceException $e) {
    $code = $e->getCode();
    $error_message = $e->getMessage();
  } 
 
} 

 
function dkan_create_table($source) {
  $tableRestProxy = AzureTableProcessor::dkan_table_store_connection();

 try {
      // Create table.
      $tableRestProxy->createTable($source);
      return TRUE;
  }
  catch (WindowsAzure\Common\ServiceException $e) {
    $code = $e->getCode();
    $error_message = $e->getMessage();
    // Handle exception based on error codes and messages.
    // Error codes and messages can be found here: 
    // http://msdn.microsoft.com/en-us/library/windowsazure/dd179438.aspx
  }

}

function dkan_table_store_connection() {
  require_once 'sites/all/libraries/vendor/autoload.php';
  $connectionString=  "DefaultEndpointsProtocol=https;AccountName=" . $this->config['azure_account_name'] . ";AccountKey=" . $this->config['azure_secret_key'];
  // Create table REST proxy.
  $connection = WindowsAzure\Common\ServicesBuilder::getInstance()->createTableService($connectionString);
  return $connection;
}


  /**
   * Return a data handler for this table.
   *
   * Avoids a call to table() to not unnecessarily instantiate DataTable.
   */
  protected function handler(FeedsSource $source) {
    return DataHandler::instance($this->tableName($source), 'id');
  }


 /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $type = node_type_get_type($this->bundle());

    $targets = parent::getMappingTargets();
  
      $targets['title'] = array(
        'name' => t('Title'),
        'description' => t('The title of the node.'),
        'optional_unique' => TRUE,
      );
    
    $targets['nid'] = array(
      'name' => t('Node ID'),
      'description' => t('The nid of the node. NOTE: use this feature with care, node ids are usually assigned by Drupal.'),
      'optional_unique' => TRUE,
    );
    $targets['uid'] = array(
      'name' => t('User ID'),
      'description' => t('The Drupal user ID of the node author.'),
    );
    $targets['user_name'] = array(
      'name' => t('Username'),
      'description' => t('The Drupal username of the node author.'),
    );
    $targets['user_mail'] = array(
      'name' => t('User email'),
      'description' => t('The email address of the node author.'),
    );
    $targets['status'] = array(
      'name' => t('Published status'),
      'description' => t('Whether a node is published or not. 1 stands for published, 0 for not published.'),
    );
    $targets['created'] = array(
      'name' => t('Published date'),
      'description' => t('The UNIX time when a node has been published.'),
    );
    $targets['promote'] = array(
      'name' => t('Promoted to front page'),
      'description' => t('Boolean value, whether or not node is promoted to front page. (1 = promoted, 0 = not promoted)'),
    );
    $targets['sticky'] = array(
      'name' => t('Sticky'),
      'description' => t('Boolean value, whether or not node is sticky at top of lists. (1 = sticky, 0 = not sticky)'),
    );

    // Include language field if Locale module is enabled.
    if (module_exists('locale')) {
      $targets['language'] = array(
        'name' => t('Language'),
        'description' => t('The two-character language code of the node.'),
      );
    }

    // Include comment field if Comment module is enabled.
    if (module_exists('comment')) {
      $targets['comment'] = array(
        'name' => t('Comments'),
        'description' => t('Whether comments are allowed on this node: 0 = no, 1 = read only, 2 = read/write.'),
      );
    }

    // If the target content type is a Feed node, expose its source field.
    if ($id = feeds_get_importer_id($this->bundle())) {
      $name = feeds_importer($id)->config['name'];
      $targets['feeds_source'] = array(
        'name' => t('Feed source'),
        'description' => t('The content type created by this processor is a Feed Node, it represents a source itself. Depending on the fetcher selected on the importer "@importer", this field is expected to be for example a URL or a path to a file.', array('@importer' => $name)),
        'optional_unique' => TRUE,
      );
    }

    // Let other modules expose mapping targets.
    self::loadMappers();
    $entity_type = $this->entityType();
    $bundle = $this->bundle();
    drupal_alter('feeds_processor_targets', $targets, $entity_type, $bundle);

    return $targets;
  }



  /**
   * At some point we might want to determine best field type in
   * the database by the type of content in the first row of each.
   * This is difficult because PHP thinks everything is a string.
   */
  function dataSchema($first_row) {
    foreach ($first_row as $field => $data) {
      // Default to bigtext for now.
      $data_schema[$field] = 'bigtext';
    }
    return $data_schema;
  }

  /**
   * Creates full schema from baseSchema and an array.
   *
   *  @param array data
   *  Arrway with keys for each desired row.
   */
  function fullSchema($data) {
    $base = $this->baseSchema();
    $data_schema = data_build_schema($data);
    $base['fields'] = array_merge($data_schema['fields'], $base['fields']);
    return $base;
  }

  /**
   * Every Feeds data table must have these elements.
   */
  protected function baseSchema() {
    return array(
      'fields' => array(
        'id' => array(
          'type' => 'serial',
          'size' => 'normal',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'timestamp' => array(
          'description' => 'The Unix timestamp for the data.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
      ),
      'indexes' => array(
        'id' => array('id'),
        'timestamp' => array('timestamp'),
       ),
       'primary key' => array(
         '0' => 'id',
       ),
    );
  }

}
